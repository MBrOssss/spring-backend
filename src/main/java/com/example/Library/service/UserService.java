package com.example.Library.service;

import com.example.Library.exception.ItemNotFoundException;
import com.example.Library.model.entity.User;
import com.example.Library.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService{

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserById(Long userId) {
        Optional<User> optionalAuthor = userRepository.findById(userId);
        return optionalAuthor.orElseThrow(() -> new ItemNotFoundException("Brak użytkownika o id: " + userId));
    }

    public void deleteUser(Long id) throws ItemNotFoundException {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        } else {
            throw new ItemNotFoundException("Brak użytkownika o id: " + id);
        }
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }



}
