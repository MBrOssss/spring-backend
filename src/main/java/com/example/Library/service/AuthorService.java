package com.example.Library.service;

import com.example.Library.exception.ItemNotFoundException;
import com.example.Library.model.entity.Author;
import com.example.Library.model.entity.Book;
import com.example.Library.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class AuthorService {

    private AuthorRepository authorRepository;

    @Autowired
    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public void deleteAuthor(Long id){
        if (authorRepository.existsById(id)) {
            authorRepository.deleteById(id);
        } else {
            throw new ItemNotFoundException("Nie można znaleźć autora o id: " + id);
        }
    }

    public void addAuthor(Author author){
        authorRepository.save(author);
    }

    public Author getAuthorById(Long id) {
        Optional<Author> optionalAuthor = authorRepository.findById(id);
        return optionalAuthor.orElseThrow(() -> new ItemNotFoundException("Nie można znaleźć autora o uuid: " + id));
    }

    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    public Set<Book> getAuthorsBooks(Long id){
        return getAuthorById(id).getBooks();
    }

}
