package com.example.Library.service;

import com.example.Library.exception.CustomBookException;
import com.example.Library.exception.ItemNotFoundException;
import com.example.Library.model.entity.Author;
import com.example.Library.model.entity.Book;
import com.example.Library.model.entity.User;
import com.example.Library.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public void deleteBook(Long id) throws ItemNotFoundException {
        if (bookRepository.existsById(id)) {
            bookRepository.deleteById(id);
        } else {
            throw new ItemNotFoundException("Can't find book with id: " + id);
        }
    }

    public void addBook(Book newBook) {
        bookRepository.save(newBook);
    }


    public void addAuthorToTheBook(Book book, Author author) {
        book.getAuthors().add(author);
        bookRepository.save(book);
    }

    public void returnBook(Long bookId, User user) throws CustomBookException {
        Book book = getBookById(bookId);
        if (book.getUser() == null) {
            throw new CustomBookException("Książka nie jest wypożyczona");
        } else if (book.getUser() != user) {
            throw new CustomBookException("Nie możesz zwrócić czyjejś książki");
        }
        book.setUser(null);
        bookRepository.save(book);
    }

    public void borrowBook(Long bookId, User user) throws CustomBookException {
        Book book = getBookById(bookId);
        if (book.getUser() != null) {
            throw new CustomBookException("Książka jest juz wypożyczona");
        }
        book.setUser(user);
        bookRepository.save(book);
    }

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public Book getBookById(Long id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        return optionalBook.orElseThrow(() -> new ItemNotFoundException("Nie można znaleźć książki o id: " + id));
    }
}
