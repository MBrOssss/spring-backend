package com.example.Library.model;

import com.example.Library.model.entity.Book;

import java.util.List;

public class BookAuthorModel {

    private Book book;
    private List<Long> idList;

    public BookAuthorModel(Book book, List<Long> idList) {
        this.book = book;
        this.idList = idList;
    }

    public Book getBook() {
        return book;
    }

    public List<Long> getIdList() {
        return idList;
    }
}
