package com.example.Library.model.entity;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
