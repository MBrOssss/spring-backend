package com.example.Library.exception;

import org.springframework.http.HttpStatus;

public class ExceptionDetails {

    private final String message;
    private final HttpStatus httpStatus;

    public ExceptionDetails(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
