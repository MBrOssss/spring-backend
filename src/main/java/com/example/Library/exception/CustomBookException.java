package com.example.Library.exception;

public class CustomBookException extends Exception{
    public CustomBookException(String message) {
        super(message);
    }
}
