package com.example.Library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalRestExceptionHandler{

    @ExceptionHandler(value = {ItemNotFoundException.class})
    public ResponseEntity<Object> handleItemNotFoundException(ItemNotFoundException e) {
        HttpStatus notFound = HttpStatus.NOT_FOUND;

        ExceptionDetails exceptionDetails = new ExceptionDetails(e.getMessage(), notFound);

        return new ResponseEntity<>(exceptionDetails, notFound);
    }

    @ExceptionHandler(value = {CustomBookException.class})
    public ResponseEntity<Object> handleBookAlreadyTakenException(CustomBookException e) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        ExceptionDetails exceptionDetails = new ExceptionDetails(e.getMessage(), status);

        return new ResponseEntity<>(exceptionDetails, status);
    }

}
