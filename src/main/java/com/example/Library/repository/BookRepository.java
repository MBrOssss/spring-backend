package com.example.Library.repository;

import com.example.Library.model.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    //Optional<Book> findById(UUID id);

    @Override
    @Query("SELECT book FROM Book book ORDER BY book.id")
    List<Book> findAll();
}
