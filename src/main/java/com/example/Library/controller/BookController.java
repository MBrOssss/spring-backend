package com.example.Library.controller;

import com.example.Library.exception.CustomBookException;
import com.example.Library.exception.ItemNotFoundException;
import com.example.Library.model.entity.Author;
import com.example.Library.model.entity.Book;
import com.example.Library.model.BookAuthorModel;
import com.example.Library.model.entity.User;
import com.example.Library.service.AuthorService;
import com.example.Library.service.BookService;
import com.example.Library.service.UserDetailsImpl;
import com.example.Library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/book")
@RestController
public class BookController {

    private final BookService bookService;
    private final AuthorService authorService;
    private final UserService userService;

    @Autowired
    public BookController(BookService bookService, AuthorService authorService, UserService userService) {
        this.bookService = bookService;
        this.authorService = authorService;
        this.userService = userService;
    }

    @PostMapping("/add")
    @PreAuthorize("hasRole('ADMIN')")
    public void addBook(@RequestBody @Valid BookAuthorModel bookAuthorModel) {
        Book book = bookAuthorModel.getBook();
        List<Long> idList = bookAuthorModel.getIdList();

        bookService.addBook(book);

        for (Long authorId : idList) {
            Author author = authorService.getAuthorById(authorId);
            bookService.addAuthorToTheBook(book, author);
        }
    }

    @DeleteMapping("delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteBook(@PathVariable("id") Long id) throws ItemNotFoundException {
        bookService.deleteBook(id);
    }

    @PutMapping(path = "/borrow/{id}")
    @PreAuthorize("hasRole('USER')")
    public void borrowBook(@PathVariable("id") Long bookId) throws CustomBookException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
        User user = userService.getUserById(userDetails.getId());

        bookService.borrowBook(bookId, user);
    }

    @GetMapping("/get/all")
    public List<Book> getAllBooks() {
        List<Book> books = bookService.getAllBooks();
        return books;
    }

    @GetMapping("/get/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Book getBookById(@PathVariable("id") Long id) {
        return bookService.getBookById(id);
    }

    @PutMapping(path = "/return/{id}")
    @PreAuthorize("hasRole('USER')")
    public void returnBook(@PathVariable("id") Long bookId) throws CustomBookException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
        User user = userService.getUserById(userDetails.getId());

        bookService.returnBook(bookId, user);
    }
}
