package com.example.Library.controller;

import com.example.Library.model.entity.Author;
import com.example.Library.model.entity.Book;
import com.example.Library.service.AuthorService;
import com.example.Library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/author")
@PreAuthorize("hasRole('ADMIN')")
@RestController
public class AuthorController {

    private final AuthorService authorService;
    private final BookService bookService;

    @Autowired
    public AuthorController(AuthorService authorService, BookService bookService) {
        this.authorService = authorService;
        this.bookService=bookService;
    }

    @PostMapping(path = "/add")
    public void addAuthor(@RequestBody @Valid Author author){
        authorService.addAuthor(author);
    }

    @DeleteMapping(path = "delete/{id}")
    public void deleteAuthor(@PathVariable("id") Long id){
        authorService.deleteAuthor(id);
    }

    @GetMapping(path = "/get/books/{id}")
    public Set<Book> getAuthorsBooks(@PathVariable("id") Long id){
        return authorService.getAuthorsBooks(id);
    }

    @GetMapping(path = "/get/all")
    public List<Author> getAll(){
        return authorService.getAllAuthors();
    }

    @GetMapping(path = "/get/{id}")
    public Author getAuthorById(@PathVariable("id") Long id){
        return authorService.getAuthorById(id);
    }
}
